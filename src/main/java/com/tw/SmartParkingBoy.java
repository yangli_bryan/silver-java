package com.tw;

public class SmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        int index = 0;
        int mostParkingSpace = 0;
        for (ParkingLot parkinglot: this.getParkingLots()) {
            if (parkinglot.getAvailableParkingPosition() > mostParkingSpace) {
                mostParkingSpace = parkinglot.getAvailableParkingPosition();
                index = this.getParkingLots().indexOf(parkinglot);
            }
        }
        if (mostParkingSpace != 0) {
            return this.getParkingLots().get(index).park(car);
        } else {
            this.setLastErrorMessage("The parking lot is full.");
            return null;
        }
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        if (ticket == null) {
            this.setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }
        int i = 0;
        while (i < this.getParkingLots().size()) {
            try {
                return this.getParkingLots().get(i).fetch(ticket);
            } catch (InvalidParkingTicketException carNotFound){
                i++;
            }
        }
        this.setLastErrorMessage("Unrecognized parking ticket.");
        return null;
    }
    // --end->
}
