package com.tw;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ParkingLot {
    private final int capacity;
    private final Map<ParkingTicket, Car> cars = new HashMap<>();

    public ParkingLot() {
        this(10);
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    ParkingTicket park(Car car) {
        Objects.requireNonNull(car);
        if (getAvailableParkingPosition() == 0) {
            throw new ParkingLotFullException();
        }
        ParkingTicket ticket = new ParkingTicket();
        cars.put(ticket, car);
        return ticket;
    }

    Car fetch(ParkingTicket ticket) {
        Objects.requireNonNull(ticket);
        if (!cars.containsKey(ticket)) {
            throw new InvalidParkingTicketException();
        }

        Car car = cars.get(ticket);
        cars.remove(ticket);
        return car;
    }

    boolean containsTicket(ParkingTicket ticket) {
        return cars.containsKey(ticket);
    }

    public int getAvailableParkingPosition() {
        return capacity - cars.size();
    }

    @SuppressWarnings("WeakerAccess")
    public int getCapacity() {
        return capacity;
    }
}
